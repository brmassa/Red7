﻿using UnityEngine;
using System.Collections.Generic;

public class List {
	
	public static void Shuffle<T> (List<T> list)
	{
        int n = list.Count;
        while (n > 1) {
            int k = Random.Range (0, n--);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
	}
}
