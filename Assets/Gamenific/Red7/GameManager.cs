﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace Gamenific.Red7
{
    class GameManager : MonoBehaviour
    {
        [SerializeField]
        Transform DrawDeckTransform = null;
        [SerializeField]
        Transform CanvasTransform = null;
        [SerializeField]
        Transform PlayersTransform = null;
        [SerializeField]
        Player PlayerPrefab = null;
        [SerializeField]
        Card CardPrefab = null;

        GameData gameData = new GameData();

        List<Card> CardsDrawDeck = new List<Card>();
        List<Card> CardsCanvas = new List<Card>();
        List<Player> Players = new List<Player>();

        #region actions
        public Action<Card, Player> OnPlayerWinnerSimulate = null;
        public Action<Player> OnPlayerNext = null;
        #endregion

        void LoadPlayer()
        {
            foreach (var playerData in gameData.Players)
            {
                var player = (Player)Instantiate(PlayerPrefab, PlayersTransform);
                Players.Add(player);
                player.Load(this, playerData, (short)Players.IndexOf(player));
            }
        }

        void LoadCards(List<CardData> cardDataList, List<Card> cardList, Transform transformList)
        {
            foreach (var cardData in cardDataList)
            {
                var card = LoadCard(cardData, cardList, transformList);
                cardList.Add(card);
            }
        }

        void PlayerNext(short playerID, PlayerData playerData)
        {
            Player playerCurrent = null;
            if (Players[playerID].playerData == playerData)
                playerCurrent = Players[playerID];
            else
            {
                foreach (var player in Players)
                {
                    if (player.playerData == playerData)
                        playerCurrent = player;
                }
            }
            if (OnPlayerNext != null)
                OnPlayerNext(playerCurrent);
        }

        void GameOver()
        {

        }

        #region public
        public void Load(GameData gameDataNew = null)
        {
            if (gameDataNew != null)
                gameData = gameDataNew;

            LoadCards(gameData.DrawDeck, CardsDrawDeck, DrawDeckTransform);
            LoadCards(gameData.Canvas, CardsCanvas, CanvasTransform);
            foreach (var card in CardsCanvas)
                card.Visible = true;
            LoadPlayer();

            gameData.OnPlayerNext += PlayerNext;

            // Set the current player
            PlayerNext(gameData.playerCurrent, gameData.Players[gameData.playerCurrent]);
        }

        public Card LoadCard(CardData cardData, List<Card> cardList, Transform transformList)
        {
            var card = (Card)Instantiate(CardPrefab, transformList);
            card.Load(cardData);
            return card;
        }

        public void Simulate(Card cardRule, bool propagate = true)
        {
            Player playerWinner = null;
            PlayerData playerDataWinner = gameData.Simulate(cardRule.cardData);

            foreach (var player in Players)
            {
                if (player.playerData == playerDataWinner) 
                {
                    playerWinner = player;
                    break;
                }
            }

            /*
            //List<CardData> validCards = new List<CardData>();
            foreach (var player in Players)
            {
                if (player.Lost)
                    continue;

                var playerValidCards = player.ValidCards(cardRule);
                if (validCards == null || CardData.CompareCards(playerValidCards, validCards) > 0)
                {
                    validCards = playerValidCards;
                    playerWinner = player;
                }
            }
            */

            if (propagate && OnPlayerWinnerSimulate != null)
                OnPlayerWinnerSimulate(cardRule, playerWinner);
        }

        public void PlayPalette(Player player, Card card)
        {
            player.playerData.CardsHand.Remove(card.cardData);
            player.playerData.CardsPalette.Add(card.cardData);

            player.CardHandRemove(card);
            player.CardPaletteAdd(card);

            player.PaletteAdded = true;
            //PlayerNext();
        }

        public void PlayCanvas(Player player, Card card)
        {
            player.playerData.CardsHand.Remove(card.cardData);
            gameData.Canvas.Add(card.cardData);

            player.CardHandRemove(card);
            CardCanvasAdd(card);
            PlayerNext();
        }

        public void PlayCanvas()
        {
            Players[gameData.playerCurrent].PlayCanvas();
        }

        [ContextMenu("Player Skip")]
        public void PlaySkip()
        {
            PlayerNext();
        }
        #endregion public

        [ContextMenu("Player Next")]
        void PlayerNext()
        {
            gameData.EndOfTurnCheck();

            gameData.PlayerNext();
        }

        void CardDrawDeckAdd(Card card)
        {
            card.transform.SetParent(DrawDeckTransform);
            CardsDrawDeck.Add(card);
        }

        void CardDrawDeckRemove(Card card)
        {
            card.transform.SetParent(null);
            CardsDrawDeck.Remove(card);
        }

        void CardCanvasAdd(Card card)
        {
            card.transform.SetParent(CanvasTransform);
            CardsCanvas.Add(card);
        }

        void CardCanvasRemove(Card card)
        {
            card.transform.SetParent(null);
            CardsCanvas.Remove(card);
        }
    }
}