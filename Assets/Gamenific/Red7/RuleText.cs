﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Gamenific.Red7
{
	class RuleText : MonoBehaviour
	{
		[SerializeField]
		GameManager gameManager = null;
		[SerializeField]
		Text text = null;
		[SerializeField]
		string[] ruleTexts;

		#region MonoBehaviour
		void Awake()
		{
			gameManager.OnPlayerWinnerSimulate += OnPlayerWinnerSimulate;
		}
		#endregion MonoBehaviour

		void OnPlayerWinnerSimulate(Card cardRule, Player playerWinner)
		{
			text.text = ruleTexts[cardRule.Color] != null ? ruleTexts[cardRule.Color] : ""; 
		}
	}
}