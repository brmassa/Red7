﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Gamenific.Red7
{
    class Card : MonoBehaviour
    {
        [SerializeField]
        Text numberCard = null;
        [SerializeField]
        Image imageCard = null;
        [SerializeField]
        Image imageSelected = null;
        [SerializeField]
        Image imageSelectable = null;
        [SerializeField]
        Color colorHidden;
        [SerializeField]
        Color colorHighlight;
        [SerializeField]
        Color[] colors;
        [SerializeField]
        Color colorsHidden;
        [SerializeField]
        Vector3 ValidOffset = new Vector3(0, 0, 1);

        [SerializeField]
        bool changeImageColor = true;

        [SerializeField]
        bool visible = false;
        [SerializeField]
        bool highlight = false;
        [SerializeField]
        bool valid = false;
        [SerializeField]
        bool selectable = false;
        [SerializeField]
        bool selected = false;
        public CardData cardData;

        public Player player;

        #region public
        public short Number
        {
            get { return cardData.Number; }
        }

        public short Color
        {
            get { return cardData.Color; }
        }

        public void Load(CardData cardDataNew)
        {
            cardData = cardDataNew;
            name = "Card " + cardData.Number + "," + cardData.Color; 
            Draw();
        }

        public void PlayCanvas()
        {
            player = null;
        }

        public bool Visible
        {
            get { return visible; }
            set
            {
                visible = value;
                Draw();
            }
        }

        public bool Highlight
        {
            get { return highlight; }
            set
            {
                highlight = value;
                Draw();
            }
        }

        public bool Selectable
        {
            get { return selectable; }
            set
            {
                selectable = value;
                Draw();
            }
        }

        public bool Selected
        {
            get { return selected; }
            set
            {
                selected = value;
                Draw();
            }
        }

        public bool Valid
        {
            get { return valid; }
            set
            {
                valid = value;
                Draw();
            }
        }

        public void Over()
        {
            if (player != null)
            {
                player.Simulate(this);
            }
        }

        public void Select()
        {
            if (player != null)
                player.CardSelected = this;
        }

        #endregion

        void Draw()
        {
            if (Visible)
            {
                if (changeImageColor)
                    imageCard.color = colors[cardData.Color];
                else
                    numberCard.color = colors[cardData.Color];
                numberCard.text = (cardData.Number + 1).ToString();
            }
            else
            {
                if (changeImageColor)
                    imageCard.color = colorHidden;
                else
                    numberCard.color = UnityEngine.Color.grey;
                numberCard.text = "?";
            }

            if (highlight)
                transform.localScale = Vector3.one * 1.1f;
            else
                transform.localScale = Vector3.one;

            if (changeImageColor)
            {
                if (valid)
                    transform.localScale = Vector3.one * 1.1f;
                else
                    transform.localScale = Vector3.one;
            }
            else
            {
                if (valid)
                    imageCard.color = colorHighlight;
                else
                    imageCard.color = UnityEngine.Color.white;
            }
                /*
            if (valid)
                transform.localPosition = ValidOffset;
            else
                transform.localPosition = Vector3.zero;
                */

            imageSelectable.enabled = Selectable;
            imageSelected.enabled = Selected;
        }
    }
}