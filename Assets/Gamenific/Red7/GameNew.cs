using UnityEngine;
using System;
using System.Collections.Generic;

namespace Gamenific.Red7
{
    [RequireComponent(typeof(GameManager))]
    class GameNew : MonoBehaviour
    {
        [SerializeField]
        bool autoInit = true;
        [SerializeField]
        short playersInit = 4;
        [SerializeField]
        short handInit = 7;
        [SerializeField]
        short paletteInit = 1;

        GameManager gameManager;
        GameData gameData = new GameData();

        #region MonoBehaviour
        void Awake()
        {
            gameManager = GetComponent<GameManager>(); 
        }

        void Start()
        {
            if (autoInit)
                GameNewFull();
        }
        #endregion MonoBehaviour

        [ContextMenu("Full New Game")]
        void GameNewFull()
        {
            gameData = new GameData();
            GameNewGenerate();
            gameManager.Load(gameData);
        }

        void GameNewGenerate()
        {
            // Create the cards
            for (short number = 0; number < 7; number++)
            {
                for (short color = 0; color < 7; color++)
                {
                    var cardData = new CardData(number, color);
                    gameData.DrawDeck.Add(cardData);
                }
            }

            // Shuffle deck
            List.Shuffle<CardData>(gameData.DrawDeck);

            // Create the players
            for (short playerID = 0; playerID < playersInit; playerID++)
            {
                var playerData = new PlayerData();
                gameData.Players.Add(playerData);

                // Fill the hand
                for (int cardID = 0; cardID < handInit; cardID++)
                {
                    var cardDataHand = gameData.DrawDeck[0];
                    playerData.CardsHand.Add(cardDataHand);
                    gameData.DrawDeck.RemoveAt(0);
                }

                // One card for the player palette
                for (int cardID = 0; cardID < paletteInit; cardID++)
                {
                    var cardDataPalette = gameData.DrawDeck[0];
                    playerData.CardsPalette.Add(cardDataPalette);
                    gameData.DrawDeck.RemoveAt(0);
                }
            }
            
            // Set the first Canvas card
            var cardDataCanvas = gameData.DrawDeck[0];
            gameData.Canvas.Add(cardDataCanvas);
            gameData.DrawDeck.RemoveAt(0);

            // Decide the first player
            var playerWinner = gameData.Simulate();
            gameData.playerCurrent = gameData.PlayerNextCheck((short)gameData.Players.IndexOf(playerWinner));
        }
    }
}