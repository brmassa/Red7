﻿using System;
using System.Collections.Generic;

namespace Gamenific.Red7
{
    [Serializable]
    class CardData
    {
        public short Number;
        public short Color;
        static ValidCardsDelegate[] Rules = new ValidCardsDelegate[]
        {
            Rule1Violet, 
            Rule2Indigo, 
            Rule3Blue,
            Rule4Green, 
            Rule5Yellow,
            Rule6Orange,
            Rule7Red
        };

        public CardData() { }
        public CardData(short numberNew, short colorNew)
        {
            Number = numberNew;
            Color = colorNew;
        }

        public static int CompareCard(CardData card1, CardData card2)
        {
            if (card1 == null) 
            {
                if (card2 == null)
                    return 1;
                else
                    return -1;
            }

            if (card2 == null
                || (card1.Number > card2.Number) 
                || (card1.Number == card2.Number && card1.Color > card2.Color)
                )
                return 1;
            else
                return -1;
        }

        public static int CompareCards(List<CardData> cards1, List<CardData> cards2)
        {
            return cards1.Count == cards2.Count
                ? CompareCard(HighestCard(cards1), HighestCard(cards2)) 
                : (cards1.Count > cards2.Count ? 1 : -1) 
                ;
        }
        
        static public List<CardData> ValidCards(CardData cardRule, List<CardData> cardList)
        {
            return Rules[cardRule.Color](cardRule, cardList);
        }
        
        public List<CardData> ValidCards(List<CardData> cardList)
        {
            return ValidCards(this, cardList);
        }

        #region CardRules
        static List<CardData> Rule1Violet(CardData cardRule, List<CardData> cardList)
        {
            List<CardData> validCards = new List<CardData>();
            foreach (var card in cardList)
                if (card.Number < 4)
                    validCards.Add(card);
            return validCards;
        }

        static List<CardData> Rule2Indigo(CardData cardRule, List<CardData> cardList)
        {
            List<CardData> validCards = new List<CardData>();
            CardData[] bestCards = new CardData[7];

            // Get a list of the best numbered cards
            foreach (var card in cardList)
            {
                if (bestCards[card.Number] == null || CompareCard(card, bestCards[card.Number]) > 0)
                    bestCards[card.Number] = card;
            }

            List<CardData> validCardsTemp = new List<CardData>();
            for (var number = 0; number < 7 + 1; number++)
            {
                if (number == 7 || bestCards[number] == null)
                {
                    if (validCardsTemp.Count >= validCards.Count)
                        validCards = validCardsTemp;

                    validCardsTemp = new List<CardData>();
                }
                else
                    validCardsTemp.Add(bestCards[number]);
            }
            return validCards;
        }

        static List<CardData> Rule3Blue(CardData cardRule, List<CardData> cardList)
        {
            List<CardData> validCards = new List<CardData>();
            CardData[] bestCards = new CardData[7];
            foreach (var card in cardList)
            {
                if (bestCards[card.Number] == null || CompareCard(card, bestCards[card.Number]) > 0)
                    bestCards[card.Number] = card;
            }

            for (var number = 0; number < 7; number++)
            {
                if (bestCards[number] != null)
                    validCards.Add(bestCards[number]);
            }
            return validCards;
        }
        
        static List<CardData> Rule4Green(CardData cardRule, List<CardData> cardList)
        {
            List<CardData> validCards = new List<CardData>();
            foreach (var card in cardList)
                if ((card.Number + 1) % 2 == 0)
                    validCards.Add(card);
            return validCards;
        }

        static List<CardData> Rule5Yellow(CardData cardRule, List<CardData> cardList)
        {
            List<CardData>[] validCards = new List<CardData>[7];
            int mostOneColor = 0;
            foreach (var card in cardList)
            {
                if (validCards[card.Color] == null)
                    validCards[card.Color] = new List<CardData> ();
                validCards[card.Color].Add(card);
            }

            for (var color = 0; color < 7; color++)
            {
                if (validCards[color] == null || validCards[color].Count == 0)
                    continue;
                if (validCards[mostOneColor] == null || validCards[color].Count > validCards[mostOneColor].Count || CompareCards(validCards[color], validCards[mostOneColor]) > 0)
                    mostOneColor = color;
            }
            return validCards[mostOneColor];
        }
        
        static List<CardData> Rule6Orange(CardData cardRule, List<CardData> cardList)
        {
            List<CardData>[] validCards = new List<CardData>[7];
            int mostOneNumber = 0;
            foreach (var card in cardList)
            {
                if (validCards[card.Number] == null)
                    validCards[card.Number] = new List<CardData> ();
                validCards[card.Number].Add(card);
            }

            for (var number = 0; number < 7; number++)
            {
                if (validCards[number] == null || validCards[number].Count == 0)
                    continue;
                if (validCards[mostOneNumber] == null || validCards[number].Count >= validCards[mostOneNumber].Count)
                    mostOneNumber = number;
            }
            return validCards[mostOneNumber];
        }

        static List<CardData> Rule7Red(CardData cardRule, List<CardData> cardList)
        {
            return new List<CardData>() {HighestCard(cardList)};
        }
        #endregion CardRules

        delegate List<CardData> ValidCardsDelegate(CardData cardRule, List<CardData> cardList);

        static CardData HighestCard(List<CardData> cardList)
        {
            CardData cardHigh = null;
            foreach (var card in cardList)
            {
                if (cardHigh == null || CompareCard(card, cardHigh) > 0)
                    cardHigh = card;
            }
            return cardHigh;
        } 
    }
}
