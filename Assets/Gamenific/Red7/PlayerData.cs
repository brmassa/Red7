﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gamenific.Red7
{
    [Serializable]
    class PlayerData
    {
        public short score = 0;
        public bool lost = false;
        public bool paletteAdded = false;
        public List<CardData> CardsHand = new List<CardData>();
        public List<CardData> CardsPalette = new List<CardData>();
        public List<CardData> CardsScore = new List<CardData>();

        public List<CardData> ValidCards(CardData cardRule)
        {
            return cardRule.ValidCards(CardsPalette);
        }
    }
}
