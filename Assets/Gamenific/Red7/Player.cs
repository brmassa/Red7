﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

namespace Gamenific.Red7
{
    class Player : MonoBehaviour
    {
        [SerializeField]
        Transform PaletteTransform = null;
        [SerializeField]
        Transform HandTransform = null;
        [SerializeField]
        List<Card> CardsPalette = new List<Card>();
        [SerializeField]
        List<Card> CardsHand = new List<Card>();
        [SerializeField]
        Image winnerIcon = null;
        [SerializeField]
        Image lostIcon = null;
        [SerializeField]
        Sprite winnerIconImage = null;
        [SerializeField]
        Sprite lostIconImage = null;
        [SerializeField]
        Image currentIcon = null;
        GameManager gameManager;
        [SerializeField]
        Button playPaletteButton = null;

        public PlayerData playerData;
        public bool HumanLocal = false;
        Card cardSelected;
        short playerID;
        bool playerCurrent = false;

        #region actions
        public Action<Card> OnCardRemove = null;
        public Action<Card> OnCardAdd = null;
        public Action<Card> OnPlayCanvas = null;
        public Action<Card> OnPlayPalette = null;
        #endregion

        #region public
        public void Load(GameManager gameManagerNew, PlayerData playerDataNew, short playerIDNew)
        {
            gameManager = gameManagerNew;
            gameManager.OnPlayerWinnerSimulate += OnPlayerWinnerSimulate;
            gameManager.OnPlayerNext += OnPlayerNext;

            playerData = playerDataNew;
            //LoadCards(playerData.CardsHand, CardsHand, HandTransform);
            //LoadCards(playerData.CardsPalette, CardsPalette, PaletteTransform);
            LoadCards(playerData.CardsHand, CardHandAdd);
            LoadCards(playerData.CardsPalette, CardPaletteAdd);

            playerID = playerIDNew;

            name = "Player " + (playerID + 1);

            foreach (var card in CardsHand)
            {
                card.player = this;
            }
            foreach (var card in CardsPalette)
            {
                card.Visible = true;
                card.player = this;
            }
        }

        void LoadCards(List<CardData> cardDataList, List<Card> cardList, Transform transformList)
        {
            foreach (var cardData in cardDataList)
            {
                var card = gameManager.LoadCard(cardData, cardList, transformList);
                cardList.Add(card);
            }
        }

        delegate void CardOperation(Card card);
        void LoadCards(List<CardData> cardDataList, CardOperation operation)
        {
            foreach (var cardData in cardDataList)
            {
                var card = gameManager.LoadCard(cardData, null, null);
                operation(card);
            }
        }

        public bool Lost
        {
            get { return playerData.lost; }
            set { playerData.lost = value; }
        }

        public bool PaletteAdded
        {
            get { return playerData.paletteAdded; }
            set { playerData.paletteAdded = value; }
        }

        public void CardHandAdd(Card card)
        {
            card.transform.SetParent(HandTransform);
            CardsHand.Add(card);
            if (OnCardAdd != null)
                OnCardAdd(card);
        }

        public void CardHandRemove(Card card)
        {
            card.transform.SetParent(null);
            CardsHand.Remove(card);
            if (OnCardRemove != null)
                OnCardRemove(card);
        }

        public void CardPaletteAdd(Card card)
        {
            card.transform.SetParent(PaletteTransform);
            CardsPalette.Add(card);
            if (OnCardAdd != null)
                OnCardAdd(card);
        }

        public void CardPaletteRemove(Card card)
        {
            card.transform.SetParent(null);
            CardsPalette.Remove(card);
            if (OnCardRemove != null)
                OnCardRemove(card);
        }

        public void PlayCanvas()
        {
            if (CardSelected != null)
                PlayCanvas(CardSelected);
        }

        public void PlayCanvas(Card CardNew)
        {
            gameManager.PlayCanvas(this, CardNew);

            playPaletteButton.interactable = false;
            CardSelected = null;

            if (OnPlayCanvas != null)
                OnPlayCanvas(CardNew);
        }

        public void PlayPalette(Card CardNew)
        {
            gameManager.PlayPalette(this, CardNew);

            CardNew.Visible = true;
            playPaletteButton.interactable = false;
            CardSelected = null;

            if (OnPlayPalette != null)
                OnPlayPalette(CardNew);
        }

        public void PlayPalette()
        {
            if (CardSelected != null)
                PlayPalette(CardSelected);
        }

        public void PlaySkip()
        {
        }

        public void PlayerTurn()
        {
        }

        public List<CardData> ValidCards(Card cardRule)
        {
            return playerData.ValidCards(cardRule.cardData);
        }

        public void Simulate(Card cardRule)
        {
            if (playerCurrent)
                gameManager.Simulate(cardRule);
        }

        public Card CardSelected
        {
            get
            {
                return cardSelected;
            }
            set
            {
                if (cardSelected != null)
                    cardSelected.Selected = false;
                
                if (value != null)
                {
                    if (!playerCurrent)
                        return;
                    cardSelected = value;
                    cardSelected.Selected = true;
                    playPaletteButton.interactable = !PaletteAdded;
                }
                else 
                    cardSelected = value;
            }
        }
        #endregion public

        void OnPlayerWinnerSimulate(Card cardRule, Player playerWinner)
        {
            if (Lost)
                return;

            var validCards = ValidCards(cardRule);
            bool validCard;
            foreach (var card in CardsPalette)
            {
                validCard = false;
                foreach (var cardData in validCards)
                {
                    if (card.cardData == cardData) 
                    {
                        card.Valid = true;
                        validCard = true;
                        validCards.Remove(cardData);
                        break;
                    }
                }
                if (!validCard)
                    card.Valid = false;
            }

            winnerIcon.enabled = playerWinner == this;
            if (playerWinner == this)
                winnerIcon.sprite = winnerIconImage;
        }

        [SerializeField]
        Image handImage;
        void OnPlayerNext(Player playerCurrentNew)
        {
            playerCurrent = playerCurrentNew == this;
            handImage.color = playerCurrent ? Color.red : Color.gray;
            currentIcon.enabled = playerCurrent;
            lostIcon.enabled = Lost;
            if (Lost)
                lostIcon.sprite = lostIconImage;

            playPaletteButton.interactable = false;

            foreach (var card in CardsHand)
                card.Visible = playerCurrent;
        }
    }
}
