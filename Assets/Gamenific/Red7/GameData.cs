﻿using System;
using System.Collections.Generic;

namespace Gamenific.Red7
{
    [Serializable]
    class GameData
    {
        public bool ruleScore = false;
        public bool ruleAction = false;
        public short playerCurrent = 0;
        public List<PlayerData> Players = new List<PlayerData>();
        public List<CardData> DrawDeck = new List<CardData>();
        public List<CardData> Canvas = new List<CardData>();

        #region actions
        public Action<short, PlayerData> OnPlayerNext = null;
        public Action OnGameOver = null;
        #endregion

        #region public
        public void PlayerNext()
        {
            var playerCurrentNew = PlayerNextCheck();
            if (playerCurrent == playerCurrentNew)
                GameOver();
            else
                playerCurrent = playerCurrentNew;

            if (OnPlayerNext != null)
                OnPlayerNext(playerCurrent, Players[playerCurrent]);
        }

        public short PlayerNextCheck(short playerCurrent)
        {
            var playerCurrentOld = playerCurrent;
            var playerCurrentNew = playerCurrent;
            while (true)
            {
                playerCurrentNew++;
                if (playerCurrentNew >= Players.Count)
                    playerCurrentNew = 0;
                if (playerCurrentNew == playerCurrentOld)
                    return playerCurrentNew;
                if (Players[playerCurrentNew].lost)
                    continue;
                break;
            }

            return playerCurrentNew;
        }

        public short PlayerNextCheck()
        {
            return PlayerNextCheck(playerCurrent);
        }

        public void EndOfTurnCheck()
        {
            Players[playerCurrent].paletteAdded = true;
            if (!ruleScore)
            {
                if (Canvas.Count == 0 || (Players[playerCurrent] != Simulate()))
                    Players[playerCurrent].lost = true;
            }
        }

        public PlayerData Simulate(CardData cardRule)
        {
            PlayerData playerWinner = null;
            List<CardData> validCards = new List<CardData>();
            foreach (var player in Players)
            {
                if (player.lost)
                    continue;

                var playerValidCards = player.ValidCards(cardRule);
                if (validCards == null || CardData.CompareCards(playerValidCards, validCards) > 0)
                {
                    validCards = playerValidCards;
                    playerWinner = player;
                }
            }

            return playerWinner;
        }


        public PlayerData Simulate()
        {
            return Simulate(Canvas[Canvas.Count - 1]);
        }
        #endregion public

        void GameOver()
        {
            if (OnGameOver != null)
                OnGameOver();
        }
    }
}
